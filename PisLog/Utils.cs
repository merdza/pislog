﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisLog
{
    public static class Utils
    {
        public static string addLeadingZeros(int number, int digitsNeeded)
        {
            string ret = number.ToString();

            //broj cifara u number
            int digitsCount = 0;
            do { digitsCount++; } while ((number /= 10) >= 1);


            for (int i = 0; i < digitsNeeded - digitsCount; i++)
            {
                ret = "0" + ret;
            }

            return ret;
        }
    }
}
