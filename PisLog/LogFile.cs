﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Drawing;

namespace PisLog
{
    public class LogFile
    {
        private static string settingsFileName = "settings.xml";
        private static string fileName = "";
        private static string logFileFullPath = "";
        private static string workstation = "";
        private static string stylesheet = "";
        private static bool testing = false;

        private static int lastLogDay;
        private static int lastLogMonth;
        private static int lastLogYear;


        private static object locker;

        private static LogFile instance = null;

        public static LogFile getInstance()
        {
            if (instance == null)
            {
                instance = new LogFile();
            }
            return instance;
        }

        private LogFile() 
        {
            workstation = XmlSettings.XmlSettings.getSetting(settingsFileName, "workstation");
            stylesheet = XmlSettings.XmlSettings.getSetting(settingsFileName, "logStylesheet");
            testing = Convert.ToBoolean(Convert.ToInt16(XmlSettings.XmlSettings.getSetting(settingsFileName, "testing")));
            setDynamicData();
            locker = new object();
        }

        public void writeLog(object logMess)
        {
            lock (locker)
            {
                try
                {
                    if (isNewDate())
                    {
                        setDynamicData();    
                    }

                    LogMessage logMessage = (LogMessage)logMess;

                    bool writeLog = true;
                    //ako je iskljuceno da se u log ispisuju dodatne informacije zbog testiranja, onda ako dodje takav dogadjaj, ne treba ga prikazivati
                    if (!testing)
                    {
                        if (logMessage.MessageType.Equals(EventType.Test))
                        {
                            writeLog = false;
                        }    
                    }

                    if (writeLog)
                    {
                        if (getCurrentLogFile().Equals(string.Empty))
                        {
                            // ne postoji fajl u direktorijumu, treba ga kreirati i napuniti prvi element
                            try
                            {
                                createLogFile();
                            }
                            catch (Exception)
                            {
                                //probaj ponovo
                                createLogFile();  //ovde nema potrebe za try-catch jer ce se proslediti do sledeceg catcha, a onda ce tamo da se utopi - vidi komentar u catchu
                            }

                        }
                        addItem(logMessage); 
                    }
                }
                catch (Exception)
                {
                    // ja ne znam sta bi ovde trebalo da se radi
                    //writeLog() se poziva spolja, ali nije problem da se tamo baci izuzetak, nego sta da se i tamo radi sa njim?
                    //jednostavno nije uspeo upis u log fajl i to je to
                }
            }
        }


        private static string getCurrentLogFile()
        {
            //da li postoji fajl sa danasnjim datumom u kom moze da se nastavi upis logova
            //ako postoji, vrati njegovo ime
            //ako ne postoji, onda vrati prazan string, pa ce se napraviti novi fajl
            string currentLogFileName = string.Empty;
            
            //gledamo pocetak imena fajla, odnosno datum i prva tri slova workstationa
            string beginingOfFile = Utils.addLeadingZeros(lastLogYear, 4) + "-" + Utils.addLeadingZeros(lastLogMonth, 2) + "-" + Utils.addLeadingZeros(lastLogDay, 2) + "-" + workstation.Substring(0,3);

            string logDirectory = XmlSettings.XmlSettings.getSetting(settingsFileName, "logLocation");

            if (Directory.Exists(logDirectory))
            {
                string[] fileEntries = (Directory.GetFiles(logDirectory, beginingOfFile + "*")); //uzmi samo fajlove koji su danasnjeg datuma
                fileEntries = fileEntries.OrderByDescending(d => d).ToArray();  //uzmi najnoviji fajl

                foreach (string file in fileEntries) //ovde verovatno ne treba foreach jer uvek uzima prvi fajl na koji naidje i iskace napolje, ali tako mi je bilo lakse
                {
                    //prvo moram da skinem putanju da bih mogao da poredim samo imena fajlova
                    string fileName = file.Replace(logDirectory, "");
                    fileName = fileName.Replace(@"\", "");
                    if (fileName.Substring(0, 14).Equals(beginingOfFile)) //datum i prva tri slova workstationa su duzine 14
                    {
                        currentLogFileName = fileName;
                        logFileFullPath = XmlSettings.XmlSettings.getSetting(settingsFileName, "logLocation") + @"\" + currentLogFileName;
                        break;
                    }
                }
            }

            return currentLogFileName;
        }

        private static bool isNewDate()
        {
            DateTime now = DateTime.Now;

            if ((now.Day != lastLogDay) || (now.Month != lastLogMonth) || (now.Year != lastLogYear))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void setDynamicData()
        {
            try
            {
                DateTime now = DateTime.Now;
                lastLogDay = now.Day;
                lastLogMonth = now.Month;
                lastLogYear = now.Year;

                fileName = Utils.addLeadingZeros(lastLogYear, 4) + "-" + Utils.addLeadingZeros(lastLogMonth, 2) + "-" + Utils.addLeadingZeros(lastLogDay, 2) + "-" + workstation + "-" + Utils.addLeadingZeros(now.Hour, 2) + "-" + Utils.addLeadingZeros(now.Minute, 2) + "-" + Utils.addLeadingZeros(now.Second, 2) + "-" + Utils.addLeadingZeros(now.Millisecond, 3);
                ///
                //fileName = Utils.addLeadingZeros(lastLogYear, 4) + "-" + Utils.addLeadingZeros(lastLogMonth, 2) + "-" + Utils.addLeadingZeros(lastLogDay, 2) + "-" + workstation;
                logFileFullPath = XmlSettings.XmlSettings.getSetting(settingsFileName, "logLocation") + @"\" + fileName + ".xml";
            }
            catch (Exception)
            {
                logFileFullPath = "";
            }
        }

        private void createLogFile()
        {
                DateTime now = DateTime.Now;
                string date = now.Day.ToString() + "." + now.Month.ToString() + "." + now.Year.ToString() + ".";

                XmlTextWriter writer = new XmlTextWriter(logFileFullPath, null);
                writer.WriteRaw(@"<?xml version='1.0'?>" +
                @"<?xml-stylesheet type='text/xsl' href='" + stylesheet + "'?>" + 
                @"<LogFile Workstation='"+ workstation +@"' Date='" + date + @"'>" +
                @"</LogFile>");
                writer.Flush();
                writer.Close();
        }

        private void createNewLogFile()
        {
            setDynamicData();
            createLogFile();
        }

        private void addItem(LogMessage logMessage)
        {
            try
            {
                DateTime now = DateTime.Now;
                string timeStamp = Utils.addLeadingZeros(now.Day, 2) + "." + Utils.addLeadingZeros(now.Month, 2) + "." + Utils.addLeadingZeros(now.Year, 4) + ". " + Utils.addLeadingZeros(now.Hour, 2) + ":" + Utils.addLeadingZeros(now.Minute, 2) + ":" + Utils.addLeadingZeros(now.Second, 2) + ":" + Utils.addLeadingZeros(now.Millisecond, 3);

                XDocument doc = XDocument.Load(logFileFullPath);

                doc.Root.Add(
                new XElement("Event",
                    new XAttribute("EventType", EventTypeExtensions.ToFriendlyString(logMessage.MessageType)),
                    new XAttribute("CardNumber", logMessage.Card),
                    new XElement("TimeStamp", timeStamp),
                    new XElement("Message", logMessage.Message))
                );
                
                doc.Save(logFileFullPath); 
            }
            catch (Exception ex)
            {
                //ako nije uspelo upisivanje u log fajl, pravi se novi fajl sa novim imenom i nastavlja se upis u njega
                createNewLogFile();
                addItem(new LogMessage(ex.Message, EventType.Error));
                addItem(logMessage);
            }
        }

        //ovako je ranije upisivao u tekstualni fajl
        //private static void addItem(string message, string eventType)
        //{
        //    try
        //    {
        //        DateTime now = DateTime.Now;
        //        string timeStamp = addDateTimeLeadingZero(now.Day) + "." + addDateTimeLeadingZero(now.Month) + "." + now.Year.ToString() + ". " + addDateTimeLeadingZero(now.Hour) + ":" + addDateTimeLeadingZero(now.Minute) + ":" + addDateTimeLeadingZero(now.Second);

        //        StreamWriter sw = File.AppendText(logFileFullPath);
        //        XmlTextWriter xtw = new XmlTextWriter(sw);
        //        xtw.Formatting = Formatting.Indented;
        //        xtw.WriteStartElement("Event");
        //        xtw.WriteAttributeString("EventType", eventType);
        //        xtw.WriteElementString("TimeStamp", timeStamp);
        //        xtw.WriteElementString("Message", message);
        //        xtw.Flush();
        //        xtw.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Nije uspelo dodavanje nove stavke u log fajl. " + ex.Message);
        //    }
        //}
    }


    public enum EventType { RegularEvent, Error, CardInserted, CardRemoved, StartProgram, ProgramClosed, Action, UpdateFromDB, ButtonClick, Test };

    public static class EventTypeExtensions
    {
        public static string ToFriendlyString(EventType et)
        {
            string ret = "";
            switch (et)
            {
                case EventType.Action:
                    ret = "Action";
                    break;
                case EventType.CardInserted:
                    ret = "CardInserted";
                    break;
                case EventType.CardRemoved:
                    ret = "CardRemoved";
                    break;
                case EventType.Error:
                    ret = "Error";
                    break;
                case EventType.ProgramClosed:
                    ret = "ProgramClosed";
                    break;
                case EventType.StartProgram:
                    ret = "StartProgram";
                    break;
                case EventType.UpdateFromDB:
                    ret = "UpdateFromDB";
                    break;
                case EventType.ButtonClick:
                    ret = "ButtonClick";
                    break;
                case EventType.Test:
                    ret = "Test";
                    break;
                default:
                    ret = "RegularEvent";
                    break;
            }
            return ret;

        }

        public static EventType ToEventType(string et)
        {
            EventType ret;
            switch (et)
            {
                case "Action":
                    ret = EventType.Action;
                    break;
                case "CardInserted":
                    ret = EventType.CardInserted;
                    break;
                case "CardRemoved":
                    ret = EventType.CardRemoved;
                    break;
                case "Error":
                    ret = EventType.Error;
                    break;
                case "ProgramClosed":
                    ret = EventType.ProgramClosed;
                    break;
                case "StartProgram":
                    ret = EventType.StartProgram;
                    break;
                case "UpdateFromDB":
                    ret = EventType.UpdateFromDB;
                    break;
                case "ButtonClick":
                    ret = EventType.ButtonClick;
                    break;
                case "Test":
                    ret = EventType.Test;
                    break;
                default:
                    ret = EventType.RegularEvent;
                    break;
            }
            return ret;
        }

        public static void GetEventsColor(EventType et, ref Color fore, ref Color back)
        {
            switch (et)
            {
                case EventType.Action:
                    back = Color.Green;
                    fore = Color.Black;
                    break;
                case EventType.CardInserted:
                    back = Color.Green;
                    fore = Color.Black;
                    break;
                case EventType.CardRemoved:
                    back = Color.Green;
                    fore = Color.Black;
                    break;
                case EventType.Error:
                    back = Color.Red;
                    fore = Color.White;
                    break;
                case EventType.ProgramClosed:
                    back = Color.Black;
                    fore = Color.White;
                    break;
                case EventType.StartProgram:
                    back = Color.Black;
                    fore = Color.White;
                    break;
                case EventType.UpdateFromDB:
                    back = Color.LightGreen;
                    fore = Color.Black;
                    break;
                case EventType.ButtonClick:
                    back = Color.LimeGreen;
                    fore = Color.Black;
                    break;
                case EventType.Test:
                    back = Color.LightSkyBlue;
                    fore = Color.Black;
                    break;
                default:
                    back = Color.LightGreen;
                    fore = Color.Black;
                    break;
            }
        }
    }

    public class LogMessage
    {
        public string Message { get; private set; }
        public EventType MessageType { get; private set; }
        public string Card { get; private set; }

        public LogMessage(string mess, EventType et)
        {
            Message = mess;
            MessageType = et;
            Card = "";
        }

        public LogMessage(string mess, EventType et, string c)
        {
            Message = mess;
            MessageType = et;
            Card = c;
        }
    }
}
